# convert Matlab mat files to csv for a given directory tree

import os
import shutil
import fnmatch
import scipy.io
import numpy as np

def convertMat(matfile,savedir):
    data = scipy.io.loadmat(matfile)
    for elem in data:
        if '__' not in elem and 'readme' not in elem:
            np.savetxt((savedir +'/'+ matfile + '_' + elem + '.csv'),data[elem],fmt='%s',delimiter=',')

# save converted csv files into ./data 
crawlDir = '/home/darwin/Desktop/machineLearning_nyu/data/crcns/'
saveDir = '/home/darwin/Desktop/machineLearning_nyu/data/data_csv/'

# recursively crawl through a given directory tree and preserve folders
for root, subFldrs, fileList in os.walk(crawlDir):
    for dirName in subFldrs:
        rel_path = os.path.relpath(root,crawlDir)
        newDir = os.path.join(saveDir,rel_path)
        # recreate directory tree
        if not os.path.exists(newDir):
            os.makedirs(newDir)
    for afile in fnmatch.filter(fileList, "*"):
        if fnmatch.filter(afile, '*.mat'):
            # convert to csv and save file
            os.chdir(root+'/'+dirName)
            convertMat(afile,newDir)
        else: 
            # copy file 
            os.chdir(root+'/'+dirName)
            shutil.copy(afile,newDir)

           
